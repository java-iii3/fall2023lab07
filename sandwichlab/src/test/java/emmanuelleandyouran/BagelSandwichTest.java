package emmanuelleandyouran;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * BagelSandwichTest is a test class for BagelSandwich
 * 
 * @author You Ran Wang
 * @version 2023/10/20
 */
public class BagelSandwichTest {

    /**
     * contructorgetterTestReturnTrue should take no input and initialize the filling field with empty string and checks if that is true
     */
    @Test
    public void contructorTestReturnTrue(){
        BagelSandwich bs = new BagelSandwich();
        assertEquals("", bs.getFilling());
    }

    /**
     * addFillingTestReturnTrue should take a string and the filling field should be updated with the filling
     */
    @Test
    public void addFillingTestReturnTrue(){
        BagelSandwich bs = new BagelSandwich();
        bs.addFilling("shadow gao");
        assertEquals("shadow gao ", bs.getFilling());
    }

    /**
     * isVegetarianFail should fail once isVegetarian is invoked on the bagelsandwich
     */
    @Test
    public void isVegetarianFail(){
        try{
            BagelSandwich bs = new BagelSandwich();
            bs.isVegetarian();
            fail("BagelSandwich does not support vegetarians");
        }catch(UnsupportedOperationException e){
            //succcess
        }

    }
}
