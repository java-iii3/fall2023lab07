package emmanuelleandyouran;

import static org.junit.Assert.*;
import org.junit.Test;

/**
* VegetarianSandwichTest is an unit testing class for the VegetarianSandwich class
* 
* @author Emmanuelle Lin
* @version 10/19/2023
*/
public class VegetarianSandwichTest {
    /**
     * isVegetarian() on VegetarianSandwich which has meat --> throw exception
     */
    @Test
    public void addFilling_fail(){
        try{
            VegetarianSandwich testVegSand = new CaesarSandwich();
            testVegSand.addFilling("lettuce");
            testVegSand.addFilling("meat");

            fail("Vegetarian sandwich shouldn't be able to have meat");

        } catch(IllegalArgumentException e){
            //success
        }
    }

    /**
     * testing getFilling() that should return String
     */
    @Test
    public void getFilling_returns_cucumber_lettuce(){
        VegetarianSandwich testVegSand = new TofuSandwich();
        testVegSand.addFilling("cucumber");
        testVegSand.addFilling("lettuce");

        assertEquals("tofu cucumber lettuce ", testVegSand.getFilling());
    }

    /**
     * testing isVegetarian() --> always return true
     */
    @Test
    public void isVegetarian_returns_true(){
        VegetarianSandwich testVegSand = new TofuSandwich();
        testVegSand.addFilling("cucumber");
        testVegSand.addFilling("lettuce");

        assertEquals(true, testVegSand.isVegetarian());
    }

    /**
     * testing isVegan() that has egg --> return false
     */
    @Test
    public void isVegan_returns_false_egg(){
        VegetarianSandwich testVegSand = new CaesarSandwich();
        testVegSand.addFilling("cucumber");
        testVegSand.addFilling("egg");

        assertEquals(false, testVegSand.isVegan());
    }

    /**
     * testing isVegan() that has milk --> return false
     */
    @Test
    public void isVegan_returns_false_milk(){
        VegetarianSandwich testVegSand = new CaesarSandwich();
        testVegSand.addFilling("cucumber");
        testVegSand.addFilling("milk");

        assertEquals(false, testVegSand.isVegan());
    }
}
