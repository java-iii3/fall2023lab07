package emmanuelleandyouran;

/**
 * VegetarianSandwhich class implements ISandwhich and contains variables: filling
 * 
 * Author: You Ran Wang
 * Version: 2023/10/17
 */
public abstract class VegetarianSandwich implements ISandwich{
    private String filling;

    /**
     * Overide for Constructor
     */
    public VegetarianSandwich(){
        this.filling = "";
    }

    /**
     * Add filling method adds a topping and rejects meats
     * @param topping
     */
    public void addFilling(String topping){
        String[] forbiddenIngrediants = {"chicken", "beef", "fish", "meat", "pork"} ;
        for(String ingrediant:forbiddenIngrediants){
            if(topping==ingrediant){
                throw new IllegalArgumentException();
            }
        }
        this.filling += topping + " ";
    }

    /**
     * getFilling shows the content of the filling field
     */
    public String getFilling(){
        return this.filling;
    }

    public abstract String getProtein();

    /**
     * isVegetarian returns wether the sandwhich is true or not in this case it always returns true
     */
    public boolean isVegetarian(){
        return true;
    }

    /**
     * isVegan checks wether the fillign string contains egg or milk and returns if it is vegan or not
     */
    public boolean isVegan(){
        if(this.filling.contains("egg") || this.filling.contains("milk")){
            return false;
        }else{
            return true;
        }
    }

}
