package emmanuelleandyouran;

/**
 * ISandwich class is a interface which contains 3 methods
 * 
 * Author: Emmanuelle Lin
 * Version: 2023/10/17
 */
public interface ISandwich 
{
    String getFilling();
    void addFilling(String topping);
    boolean isVegetarian();
    
}
