package emmanuelleandyouran;

/**
 * Tofu sandwich class extends the vegesandwich class and also has a getProtein method and has a different constructor
 * 
 * Author: You Ran Wang
 * Version: 2023/10/20
 */
public class TofuSandwich extends VegetarianSandwich{

    /**
     * Contructor overide always creates a filling with tofu in it
     */
    public TofuSandwich(){
        super.addFilling("tofu");
    }

    /**
     * Overide of get protein which returns tofu
     */
    @Override
    public String getProtein(){
        return "tofu";
    }


}
