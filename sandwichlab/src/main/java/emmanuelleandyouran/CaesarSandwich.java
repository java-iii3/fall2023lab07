package emmanuelleandyouran;

/**
* CaesarSandwich class extends VegetarianSandwhich
* 
* @author Emmanuelle Lin
* @version 20/10/2023
*/
public class CaesarSandwich extends VegetarianSandwich{
    
    /**
     * Constructor overrides and adds Caesar dressing to field filling
     */
    public CaesarSandwich(){
        super.addFilling("Caesar dressing");
    }

    /**
     * method getProtein overriden and returns anchovies
     * 
     * @return Anchovies
     */
    @Override
    public String getProtein(){
        return "Anchovies";
    }

    /**
     * method isVegetarian overriden and always returns false
     * 
     * @return false
     */
    @Override
    public boolean isVegetarian(){
        return false;
    }
}
