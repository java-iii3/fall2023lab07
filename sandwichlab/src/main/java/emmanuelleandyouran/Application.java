package emmanuelleandyouran;

public class Application {
    public static void main(String[] args){
        //VegetarianSandwich test = new VegetarianSandwich();
        ISandwich samich = new TofuSandwich();
        if(samich instanceof ISandwich){
            System.out.println("yes, ISandwich");
        }
        if(samich instanceof VegetarianSandwich){
            System.out.println("yes, VegetarianSandwich");
        }

        //samich.addFilling("chicken");
        //samich.isVegan();
        // VegetarianSandwich castedSamich = (VegetarianSandwich)samich;
        // castedSamich.isVegan();

        VegetarianSandwich otherSamich = new CaesarSandwich();
        System.out.println(otherSamich.isVegetarian());
    }
}
