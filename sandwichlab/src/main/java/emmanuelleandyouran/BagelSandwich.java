package emmanuelleandyouran;

/** BagelSandwich is a class used for making a bagel sandwich. It adds filling and gets filling.
 * 
 * @author Emmanuelle Lin
 * @version 10/17/2023
 */
public class BagelSandwich implements ISandwich{
    private String filling;
    
    /**
     * Override of constructor
     */
    public BagelSandwich(){
        this.filling = "";
    }

    /** 
     * Adds the parameter topping to the field filling
     * 
     * @param topping The word to be added to the filling field
     * @return void
    */
    public void addFilling(String topping){
        this.filling += topping + " ";
    }

    /** 
     * Gets the private field filling 
     * 
     * @return field filling
    */
    public String getFilling(){
        return this.filling;
    }

    /** 
     * Returns a boolean based on if the sandwich is vegetarian or not
     * 
     * @return true or false
    */
    public boolean isVegetarian(){
        throw new UnsupportedOperationException("Might not be Vegetarian");
    }
}
